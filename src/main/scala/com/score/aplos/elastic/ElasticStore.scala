package com.score.aplos.elastic

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.{GET, PUT}
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.score.aplos.config.{CassandraConf, ElasticConf}
import com.score.aplos.util.AppLogger

import scala.concurrent.Await
import scala.concurrent.duration._

object ElasticStore extends AppLogger with CassandraConf with ElasticConf {

  def init()(implicit system: ActorSystem): Unit = {
    System.setProperty("es.set.netty.runtime.available.processors", "false")

    initIndex(transElasticIndex, transElasticDocType)
    initIndex(accountsElasticIndex, accountsElasticDocType)
  }

  def initIndex(index: String, docTyp: String)(implicit system: ActorSystem): Unit = {
    implicit val ec = system.dispatcher
    implicit val materializer = ActorMaterializer()
    implicit val timeout = 40.seconds

    // params
    val uri = s"http://${elasticHosts.head}:9200/$index"
    val json =
      s"""
      {
        "settings":{
          "keyspace": "$cassandraKeyspace"
        },
        "mappings": {
          "$docTyp" : {
            "discover" : ".*"
          }
        }
      }
    """

    // check index exists
    val get = HttpRequest(GET, uri = uri)
    Await.result(Http().singleRequest(get), timeout).status match {
      case StatusCodes.NotFound =>
        // index not exists
        logger.info(s"init index request uri $uri json $json")

        // create index
        val put = HttpRequest(PUT, uri = uri).withEntity(ContentTypes.`application/json`, ByteString(json.stripLineEnd))
        val resp = Await.result(Http().singleRequest(put), timeout)

        logger.info(s"init index response: $resp")
      case _ =>
        // index already exists
        logger.info(s"$index index already exists")
    }
  }

}

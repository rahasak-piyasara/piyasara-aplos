package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.Sink
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import com.score.aplos.actor.AplosActor.Stream
import com.score.aplos.actor.FlightActor.Create
import com.score.aplos.config.KafkaConf
import com.score.aplos.protocol.FlightMessage
import com.score.aplos.util.{AppLogger, DateFactory}
import org.apache.kafka.common.serialization.StringDeserializer
import spray.json._

object AplosActor {

  case class Stream()

  def props() = Props(new AplosActor)

}

class AplosActor() extends Actor with AppLogger with KafkaConf {

  override def receive: Receive = {
    case Stream =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(context.system).withSupervisionStrategy(decider))
      implicit val ec = context.system.dispatcher

      // kafka source
      val consumerSettings = ConsumerSettings(context.system, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(kafkaAddr)
        .withGroupId(kafkaGroup)
      val source = Consumer.committableSource(consumerSettings, Subscriptions.topics(kafkaTopic))

      // consumer as stream
      source
        .map(kmsg => {
          // got the message
          val msg = kmsg.record.value
          logger.info(s"got actor message - $msg")

          import com.score.aplos.protocol.FlightMessageProtocol._
          val accMsg = msg.parseJson.convertTo[FlightMessage]

          accMsg match {
            case create: Create =>
              context.actorOf(FlightActor.props()) ! create
          }
        })
        .runWith(Sink.ignore)
  }

}

//object M extends App {
//  val msg =
//    """
//      |{"messageType":"create", "execer":"eranga", "id":"1111", "flightId":"1188", "flightNo":"UL120", "flightTime":"2019-02-19 10:47:15", "flightStatus":"Done", "flightFrom":"SL", "flightTo":"SG", "flightEvent":"GROOP"} """.stripMargin
//
//
//  import com.score.aplos.protocol.FlightMessageProtocol._
//  val accMsg = msg.parseJson.convertTo[FlightMessage]
//
//  accMsg match {
//    case create: Create =>
//      println("joooo")
//      println(DateFactory.formatToDate(create.flightTime, DateFactory.TIMESTAMP_FORMAT))
//  }
//}

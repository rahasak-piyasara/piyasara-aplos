package com.score.aplos.actor

import akka.actor.{Actor, Props}
import com.score.aplos.actor.FlightActor._
import com.score.aplos.cassandra.{CassandraStore, Trans}
import com.score.aplos.protocol.FlightMessage
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

object FlightActor {

  case class Create(messageType: String, execer: String, id: String, flightId: String, flightNo: String, flightTime: String,
                    flightStatus: String, flightFrom: String, flightTo: String, flightEvent: String) extends FlightMessage

  def props() = Props(new FlightActor)

}

class FlightActor extends Actor with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // first check double spend
      val id = s"${create.execer};${create.id}"
      if (!CassandraStore.isDoubleSpend(create.execer, create.id) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Create] = jsonFormat10(Create)
        val trans = Trans(create.id, create.execer, "FlightActor", create.toJson.toString,
          create.flightId, create.flightNo, DateFactory.formatToDate(create.flightTime, DateFactory.DATE_FORMAT),
          create.flightStatus, create.flightFrom, create.flightTo, create.flightEvent)
        CassandraStore.createTrans(trans)

        logger.info(s"create done - $create")
      } else {
        logger.error(s"double spend create - $create")
      }
  }
}

package com.score.aplos.protocol

import com.score.aplos.actor.FlightActor._
import spray.json._

trait FlightMessage

object FlightMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat10(Create)

  implicit object FlightMessageProtocol extends RootJsonFormat[FlightMessage] {
    def write(obj: FlightMessage): JsValue =
      JsObject((obj match {
        case i: Create => i.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): FlightMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

